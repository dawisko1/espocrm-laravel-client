<?php
/*
|--------------------------------------------------------------------------
| EspoCRM Settings
|--------------------------------------------------------------------------
| Damn I love this type of comment :D
*/
return [
    /*
    |--------------------------------------------------------------------------
    | EspoCRM Instance URL
    |--------------------------------------------------------------------------
    |
    | This URL is used to tell the client where to send the requests, point this
    | to your EspoCRM instance.
    |
    */
    "url" => env( "ESPOCRM_URL", "" ),
    /*
    |--------------------------------------------------------------------------
    | EspoCRM Authentication Data
    |--------------------------------------------------------------------------
    |
    | Define your preferred authentication mechanism with EspoCRM, as well as
    | the required keys/authentication info required for that method.
    |
    | Please approach with care and:
    | 1) Create a specific user or api user and limit their capabilities to
    | minimize the risks.
    | 2) Never, ever specify access data for admin or any role that can
    | access/edit/compromise your data!
    */
    "auth" => [
        /*
        |--------------------------------------------------------------------------
        | EspoCRM Authentication Type
        |--------------------------------------------------------------------------
        |
        | Available authentication modes up to date are:
        | HMAC - requires api_key and secret_key
        | API - requires only the api_key
        | BASIC - requires the user and password
        |
        | Note: usage of the basic method is discouraged.
        */
        "type" => env( "ESPOCRM_AUTH_TYPE", "HMAC" ),
        /*
        |--------------------------------------------------------------------------
        | EspoCRM API & secret
        |--------------------------------------------------------------------------
        | Used for the API and HMAC authentication modes.
        */
        "secret_key" => env( "ESPOCRM_SECRET" ),
        "api_key" => env( "ESPOCRM_KEY" ),
        /*
        |--------------------------------------------------------------------------
        | EspoCRM User & Password
        |--------------------------------------------------------------------------
        | Used for the BASIC authentication mode (discouraged)
        */
        "user" => env( "ESPOCRM_USER" ),
        "password" => env( "ESPOCRM_PASSWORD" ),
    ],
    /*
    |--------------------------------------------------------------------------
    | EspoCRM Package Settings
    |--------------------------------------------------------------------------
    */
    "settings" => [
        /*
        |--------------------------------------------------------------------------
        | EspoCRM Where Query Available Operations
        |--------------------------------------------------------------------------
        */
        "availableWhereQueries" => [
            "arrayAllOf" => [
                /* Define whether the operation expects a value to be passed to it */
                "expectsValue" => true,
            ],
            "arrayNoneOf" => [
                "expectsValue" => true,
            ],
            "arrayAnyOf" => [
                "expectsValue" => true,
            ],
            "between" => [
                "expectsValue" => true,
            ],
            "afterXDays" => [
                "expectsValue" => true,
            ],
            "olderThanXDays" => [
                "expectsValue" => true,
            ],
            "nextXDays" => [
                "expectsValue" => true,
            ],
            "lastXDays" => [
                "expectsValue" => true,
            ],
            "arrayIsNotEmpty" => [
                "expectsValue" => false,
            ],
            "arrayIsEmpty" => [
                "expectsValue" => false,
            ],
            "lastFiscalQuarter" => [
                "expectsValue" => false,
            ],
            "currentFiscalQuarter" => [
                "expectsValue" => false,
            ],
            "lastFiscalYear" => [
                "expectsValue" => false,
            ],
            "currentFiscalYear" => [
                "expectsValue" => false,
            ],
            "lastYear" => [
                "expectsValue" => false,
            ],
            "currentYear" => [
                "expectsValue" => false,
            ],
            "lastQuarter" => [
                "expectsValue" => false,
            ],
            "currentQuarter" => [
                "expectsValue" => false,
            ],
            "nextMonth" => [
                "expectsValue" => false,
            ],
            "lastMonth" => [
                "expectsValue" => false,
            ],
            "currentMonth" => [
                "expectsValue" => false,
            ],
            "lastSevenDays" => [
                "expectsValue" => false,
            ],
            "future" => [
                "expectsValue" => false,
            ],
            "today" => [
                "expectsValue" => false,
            ],
            "isLinked" => [
                "expectsValue" => false,
            ],
            "isNotLinked" => [
                "expectsValue" => false,
            ],
            "past" => [
                "expectsValue" => false,
            ],
            "equalTo" => [
                "expectsValue" => true,
            ],
            "notEqualTo" => [
                "expectsValue" => true,
            ],
            "greaterThan" => [
                "expectsValue" => true,
            ],
            "lessThan" => [
                "expectsValue" => true,
            ],
            "greaterOrEqualTo" => [
                "expectsValue" => true,
            ],
            "lessOrEqualTo" => [
                "expectsValue" => true,
            ],
            "isNull" => [
                "expectsValue" => true,
            ],
            "isNotNull" => [
                "expectsValue" => true,
            ],
            "isTrue" => [
                "expectsValue" => true,
            ],
            "isFalse" => [
                "expectsValue" => true,
            ],
            "isLinkedWith" => [
                "expectsValue" => true,
            ],
            "isNotLinkedWith" => [
                "expectsValue" => true,
            ],
            "in" => [
                "expectsValue" => true,
            ],
            "notIn" => [
                "expectsValue" => true,
            ],
            "contains" => [
                "expectsValue" => true,
            ],
            "notContains" => [
                "expectsValue" => true,
            ],
            "startsWith" => [
                "expectsValue" => true,
            ],
            "endsWith" => [
                "expectsValue" => true,
            ],
            "isNotLike" => [
                "expectsValue" => true,
            ],
            "isLike" => [
                "expectsValue" => true,
            ],
        ],
        /*
         |--------------------------------------------------------------------------
         | EspoCRM Defaults
         |--------------------------------------------------------------------------
         | This setting allows you to create a default data template for all crud
         | operations (create, update as only those two require data) of all or
         | particular entities.
         | Note, however that if both entity and generic entries exist they will be
         | merged (e.g.: create and Issue.create will be merged crete <- Issue.create)
         |
         | Use to for instance limit the task creation to specific client or user
         | responsible for handling the tasks.
         */
        "defaults" => [
            
        ]
    ]
];
