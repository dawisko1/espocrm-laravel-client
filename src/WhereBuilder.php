<?php

namespace LaravelEspo;

/**
 * Class WhereBuilder
 * @package App\Domain\EspoCRM
 * @method arrayAllOf($attribute, $value)
 * @method arrayNoneOf($attribute, $value)
 * @method arrayAnyOf($attribute, $value)
 * @method between($attribute, $value)
 * @method afterXDays($attribute, $value)
 * @method olderThanXDays($attribute, $value)
 * @method nextXDays($attribute, $value)
 * @method lastXDays($attribute, $value)
 * @method arrayIsNotEmpty($attribute)
 * @method arrayIsEmpty($attribute)
 * @method lastFiscalQuarter($attribute)
 * @method currentFiscalQuarter($attribute)
 * @method lastFiscalYear($attribute)
 * @method currentFiscalYear($attribute)
 * @method lastYear($attribute)
 * @method currentYear($attribute)
 * @method lastQuarter($attribute)
 * @method currentQuarter($attribute)
 * @method nextMonth($attribute)
 * @method lastMonth($attribute)
 * @method currentMonth($attribute)
 * @method lastSevenDays($attribute)
 * @method future($attribute)
 * @method today($attribute)
 * @method isLinked($attribute)
 * @method isNotLinked($attribute)
 * @method past($attribute)
 * @method equalTo($attribute, $value)
 * @method notEqualTo($attribute, $value)
 * @method greaterThan($attribute, $value)
 * @method lessThan($attribute, $value)
 * @method greaterOrEqualTo($attribute, $value)
 * @method lessOrEqualTo($attribute, $value)
 * @method isNull($attribute, $value)
 * @method isNotNull($attribute, $value)
 * @method isTrue($attribute, $value)
 * @method isFalse($attribute, $value)
 * @method isLinkedWith($attribute, $value)
 * @method isNotLinkedWith($attribute, $value)
 * @method in($attribute, $value)
 * @method notIn($attribute, $value)
 * @method contains($attribute, $value)
 * @method notContains($attribute, $value)
 * @method startsWith($attribute, $value)
 * @method endsWith($attribute, $value)
 * @method isNotLike($attribute, $value)
 * @method isLike($attribute, $value)
 */
class WhereBuilder {
    /**
     * @var array
     */
    protected $availableOperands;

    /**
     * @var array
     */
    private $resultQuery = [ ];
    /**
     * @var null
     */
    private $currentAttribute = null;
    /**
     * @var null
     */
    private $currentOrQuery = null;

    /**
     * WhereBuilder constructor.
     */
    public function __construct()
    {
        $this->availableOperands = config( "espocrm.settings.availableWhereQueries" );
    }


    /**
     * @param $name
     * @return WhereBuilder
     */
    public function __get($name) {
        $this->resultQuery[ $this->currentAttribute ][] = $this->currentOrQuery;

        if ( array_key_exists( $name, $this->resultQuery ) ) {
            return $this->setCurrentAttribute($name);
        }
        else {
            $this->resultQuery[ $name ] = [  ];
            return $this->setCurrentAttribute($name);
        }
    }

    /**
     * @param $name
     * @param $arguments
     * @return $this
     * @throws \Exception
     */
    public function __call($name, $arguments ) {
        if( strtolower( substr( $name, 0, 2 ) ) === "or" ) {
            $this->addToOrQuery( $name, $arguments );
        }

        if( array_key_exists( $name, $this->availableOperands ) ) {
            if( $this->checkShouldUseValue( $name ) && empty( $arguments ) )
                throw new \Exception( "The $name query expects a value constraint" );

            $this->resultQuery[ $this->currentAttribute ][] =
                $this->getTypeDef( $name, $arguments[ 0 ] ?? false );
        }

        return $this;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setCurrentAttribute($name) {
        $this->currentAttribute = $name;

        return $this;
    }

    /**
     * @param $name
     * @param bool $value
     * @return array
     */
    protected function getTypeDef($name, $value = false)
    {
        $_typeDef = [
            "type" => $name,
            "attribute" => $this->currentAttribute,
        ];

        if( !$value )
            $_typeDef[ 'value' ] = $value;

        return $_typeDef;
    }

    /**
     * @param $name
     * @param $arguments
     * @throws \Exception
     */
    private function addToOrQuery($name, $arguments)
    {
        $name = substr( $name, 2 );

        if( is_null( $this->currentOrQuery ) ) {
            $this->currentOrQuery = [
                "type" => "or",
                "value" => [  ],
            ];
        }

        if ( $this->checkShouldUseValue( $name ) && empty( $arguments ) )
            throw new \Exception( "Query $name expects a value constraint" );

        $this->currentOrQuery[ 'value' ][] = $this->getTypeDef( $name, $arguments[ 0 ] ?? false );
    }

    /**
     * @param $name
     * @return mixed
     */
    private function checkShouldUseValue($name ) {
        return $this->availableOperands[ $name ][ 'expectsValue' ];
    }

    /**
     * @return array
     */
    public function getResultQuery(): array
    {
        return $this->resultQuery;
    }
}
