<?php

namespace LaravelEspo;

/**
 * Class Client
 * @package App\Domain\EspoCRM
 */

class Client
{
    /**
     * @var null
     */
    private $url;
    /**
     * @var null
     */
    private $userName = null;
    /**
     * @var null
     */
    private $password = null;
    /**
     * @var string
     */
    protected $urlPath = '/api/v1/';
    /**
     * @var
     */
    private $lastCh;
    /**
     * @var
     */
    private $lastResponse;
    /**
     * @var null
     */
    private $apiKey = null;
    /**
     * @var null
     */
    private $secretKey = null;

    /**
     * Client constructor.
     * @param null $url
     * @param null $userName
     * @param null $password
     */
    public function __construct($url = null, $userName = null, $password = null)
    {
        if (isset($url)) {
            $this->url = $url;
        }
        if (isset($userName)) {
            $this->userName = $userName;
        }
        if (isset($password)) {
            $this->password = $password;
        }
    }

    // region Setters

    /**
     * @param $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @param $userName
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
    }

    /**
     * @param $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @param $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @param $secretKey
     */
    public function setSecretKey($secretKey)
    {
        $this->secretKey = $secretKey;
    }
    // endregion
    // region Getters

    /**
     * @return mixed
     */
    public function getResponseContentType()
    {
        return $this->getInfo(CURLINFO_CONTENT_TYPE);
    }

    /**
     * @return mixed
     */
    public function getResponseTotalTime()
    {
        return $this->getInfo(CURLINFO_TOTAL_TIME);
    }

    /**
     * @return mixed
     */
    public function getResponseHttpCode()
    {
        return $this->getInfo(CURLINFO_HTTP_CODE);
    }
    // endregion
    // region Logic
    /**
     * @param $action
     * @return string
     */
    protected function normalizeUrl($action)
    {
        return $this->url . $this->urlPath . $action;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    protected function checkParams()
    {
        $paramList = [
            'url'
        ];

        foreach ($paramList as $name) {
            if (empty($this->$name)) {
                throw new \Exception('EspoClient: Parameter "'.$name.'" is not defined.');
            }
        }

        return true;
    }

    /**
     * @param $option
     * @return mixed|null
     */
    protected function getInfo($option)
    {
        if (isset($this->lastCh)) {
            return curl_getinfo($this->lastCh, $option);
        }

        return null;
    }

    /**
     * @param $response
     * @return array
     */
    protected function parseResponse($response)
    {
        $headerSize = $this->getInfo(CURLINFO_HEADER_SIZE);

        return [
            'header' => trim( substr($response, 0, $headerSize) ),
            'body' => substr($response, $headerSize),
        ];
    }

    /**
     * @param $header
     * @return array
     */
    protected function normalizeHeader($header)
    {
        preg_match_all('/(.*?): (.*)\r\n/', $header, $matches);

        $headerArray = array();
        foreach ($matches[1] as $index => $name) {
            if (isset($matches[2][$index])) {
                $headerArray[$name] = trim($matches[2][$index]);
            }
        }

        return $headerArray;
    }
    //endregion

    /**
     * Send request to EspoCRM
     *
     * @param string $method
     * @param string $action
     * @param array|null $data
     *
     * @return array | \Exception
     * @throws \Exception
     */
    public function request($method, $action, array $data = null)
    {
        $method = strtoupper($method);

        $this->checkParams();

        $this->lastResponse = null;
        $this->lastCh = null;

        $url = $this->normalizeUrl($action);

        $ch = curl_init($url);
        $headerList = [];

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($this->userName && $this->password) {
            curl_setopt($ch, CURLOPT_USERPWD, $this->userName.':'.$this->password);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        } else if ($this->apiKey && $this->secretKey) {
            $string = $method . ' /' . $action;
            $authPart = base64_encode($this->apiKey . ':' . hash_hmac('sha256', $string, $this->secretKey, true));
            $authHeader = 'X-Hmac-Authorization: ' .  $authPart;
            $headerList[] = $authHeader;
        } else if ($this->apiKey) {
            $authHeader = 'X-Api-Key: ' .  $this->apiKey;
            $headerList[] = $authHeader;
        }

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, true);

        if ($method != 'GET') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        }

        if (isset($data)) {
            if ($method == 'GET') {
                curl_setopt($ch, CURLOPT_URL, $url. '?' . http_build_query($data));
            } else {
                $payload = json_encode($data);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
                $headerList[] = 'Content-Type: application/json';
                $headerList[] = 'Content-Length: ' . strlen($payload);
            }
        }

        if (!empty($headerList)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headerList);
        }

        $this->lastResponse = curl_exec($ch);
        $this->lastCh = $ch;

        $parsedResponse = $this->parseResponse($this->lastResponse);
        $responseCode = $this->getResponseHttpCode();
        $responseContentType = $this->getResponseContentType();

        switch ( $responseCode ) {
            case 200:
                curl_close($ch);

                if ($responseContentType === 'application/json') {
                    return json_decode($parsedResponse['body'], true);
                }

                return $parsedResponse['body'];
            break;
            case 401:
                curl_close($ch);
                throw new \Exception("You are not authorized or your authority level is too low to perform this action.");
            case 500:
                curl_close($ch);
                throw new \Exception("An error occurred at target instance and your request could not be served");
            case 404:
                curl_close($ch);
                throw new \Exception("The request failed with status 404, please check your URL and URI's or make sure the resource exists.");
            default:
                $header = $this->normalizeHeader($parsedResponse['header']);
                $errorMessage = !empty($header['X-Status-Reason']) ? $header['X-Status-Reason'] : 'EspoClient: An Unknown Error Occurred';

                curl_close($ch);
                throw new \Exception($errorMessage, $responseCode);
        }
    }
}
