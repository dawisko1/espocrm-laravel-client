<?php

namespace LaravelEspo\Providers;

use LaravelEspo\Client;
use Illuminate\Support\ServiceProvider;

class EspocrmAPIProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind( Client::class, function ($app) {
            $client = new Client( $app[ 'config' ][ 'espocrm' ][ 'url' ] );

            switch ( strtolower( $app[ 'config' ][ 'espocrm' ][ 'auth' ][ 'type' ] ) ) {
                default:
                case "hmac":
                    $client->setApiKey( $app[ 'config' ][ 'espocrm' ][ 'auth' ][ 'api_key' ] );
                    $client->setSecretKey( $app[ 'config' ][ 'espocrm' ][ 'auth' ][ 'secret_key' ] );
                    break;
                case "basic":
                    $client->setUserName( $app[ 'config' ][ 'espocrm' ][ 'auth' ][ 'user' ] );
                    $client->setPassword( $app[ 'config' ][ 'espocrm' ][ 'auth' ][ 'password' ] );
                    break;
                case "api":
                    $client->setApiKey( $app[ 'config' ][ 'espocrm' ][ 'auth' ][ 'api_key' ] );
                    break;
            }

            return $client;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/espocrm.php' => config_path('espocrm.php'),
        ]);
    }
}
