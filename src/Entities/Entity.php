<?php

namespace LaravelEspo\Entities;

use LaravelEspo\Client;
use LaravelEspo\Contracts\EntityContract;
use LaravelEspo\WhereBuilder;

/**
 * Class Entity
 * @package App\Domain\EspoCRM\Entities
 * @method static new(array $data)
 * @method static get(string $id)
 * @method static amend(string $id, array $data)
 * @method static destroy(string $id)
 * @method static list()
 */
class Entity implements EntityContract {
    /**
     * @var array
     */
    protected static $MethodMapping = [
        "new" => "create",
        "get" => "read",
        "destroy" => "delete",
        "amend" => "update",
        "list" => "index",
    ];
    /**
     * @var string $entityURI override this to target different entity types.
     */
    protected $entityURI = "";

    /**
     * @var string $entityName Set this to the entity name for defaults lookup
     */
    protected $entityName = "";

    /**
     * @var int|null set to null if you don't want to use
     */
    private $maxSize = null;

    /**
     * @var int|null set to null if you don't want to use
     */
    private $offset = null;

    /**
     * @var WhereBuilder|null set to null if you don't want to use
     */
    private $whereQuery = null;

    /**
     * @var string|null set to null if you don't want to use
     */
    private $orderBy = null;

    /**
     * @var string|null set to null if you don't want to use
     */
    private $order = null;

    /**
     * @var array|null set to null if you don't want to use
     */
    private $select = null;

    /**
     * @var Client|null
     */
    private $client = null;

    /**
     * Entity constructor.
     * @param Client $client
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct()
    {
        $this->client = app()->make( Client::class );
    }

    /**
     * @param array $data
     * @return string returns the entity id
     * @throws \Exception
     */
    public function create(array $data)
    {
        $defaults = config("espocrm.settings.defaults.create", [ ]);

        if( !empty( $this->entityName ) )
            $entityDefaults = config("espocrm.settings.defaults." . $this->entityName . ".create", [ ]);

        if( !empty( $defaults ) && isset( $entityDefaults ) )
            $defaults = array_merge( $defaults, $entityDefaults );

        $result = $this->client->request( "POST", $this->entityURI, array_merge( $defaults, $data ) );

        return $result[ "id" ];
    }

    /**
     * @param $id
     * @param array $data
     * @return bool
     */
    public function update($id, array $data)
    {
        try {
            $defaults = config("espocrm.settings.defaults.update", [ ]);

            if( !empty( $this->entityName ) )
                $entityDefaults = config("espocrm.settings.defaults." . $this->entityName . ".update", [ ]);

            if( !empty( $defaults ) && isset( $entityDefaults ) )
                $defaults = array_merge( $defaults, $entityDefaults );

            $this->client->request( "PUT", $this->entityURI . "/$id", array_merge( $defaults, $data ) );
            return true;
        }
        catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $id
     * @return bool whether the entity was deleted or not
     */
    public function delete($id)
    {
        try {
            $result = $this->client->request( "DELETE", $this->entityURI . "/$id" );

            if ( is_array( $result ) )
                return $result[ 0 ];
            else
                return true;
        }
        catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $id
     * @return array
     * @throws \Exception
     */
    public function read($id)
    {
        return $this->client->request( "GET", $this->entityURI . "/$id" );
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function index()
    {
        $query = [ ];

        if( !is_null( $this->maxSize ) )
            $query[ "maxSize" ] = $this->maxSize;

        if( !is_null( $this->offset ) )
            $query[ "offset" ] = $this->offset;

        if( !is_null( $this->orderBy ) )
            $query[ "orderBy" ] = $this->orderBy;

        if( !is_null( $this->order ) )
            $query[ "order" ] = $this->order;

        if( !is_null( $this->select ) )
            $query[ "select" ] = implode(",", $this->select);

        if( !is_null( $this->whereQuery ) )
            $query[ "where" ] = $this->whereQuery;

        $query = http_build_query( $query );

        return $this->client->request( "GET", $this->entityURI . "?$query" );
    }

    /**
     * @param WhereBuilder|array $query
     * @return Entity
     */
    public function where($query)
    {
        if( is_a( $query, WhereBuilder::class ) ) {
            $this->whereQuery = $query->getResultQuery();
        }
        else {
            $this->whereQuery = $query;
        }

        return $this;
    }

    /**
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public static function __callStatic($name, $arguments)
    {
        if( array_key_exists( $name, self::$MethodMapping ) ) {
            $func = self::$MethodMapping[ $name ];

            return (new static)->$func( ...$arguments );
        }
    }
}
