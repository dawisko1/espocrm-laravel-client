<?php

namespace LaravelEspo\Entities;

/**
 * Class Issue
 * @package LaravelEspo\Entities
 */
class Issue extends Entity {
    /**
     * @var string
     */
    protected $entityURI = "Case";

    /**
     * @var string
     */
    protected $entityName = "Case";
}
