<?php

namespace LaravelEspo\Entities;

/**
 * Class Meeting
 * @package LaravelEspo\Entities
 */
class Meeting extends Entity {
    /**
     * @var string
     */
    protected $entityURI = "Meeting";

    /**
     * @var string
     */
    protected $entityName = "Meeting";
}
