<?php

namespace LaravelEspo\Entities;

/**
 * Class Campaign
 * @package LaravelEspo\Entities
 */
class Campaign extends Entity {
    /**
     * @var string
     */
    protected $entityURI = "Campaign";

    /**
     * @var string
     */
    protected $entityName = "Campaign";
}
