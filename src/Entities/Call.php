<?php

namespace LaravelEspo\Entities;

/**
 * Class Call
 * @package LaravelEspo\Entities
 */
class Call extends Entity {
    /**
     * @var string
     */
    protected $entityURI = "Call";
    /**
     * @var string
     */
    protected $entityName = "Call";
}
