<?php

namespace LaravelEspo\Entities;

/**
 * Class Task
 * @package LaravelEspo\Entities
 */
class Task extends Entity {
    /**
     * @var string
     */
    protected $entityURI = "Task";

    /**
     * @var string
     */
    protected $entityName = "Task";
}
