<?php

namespace LaravelEspo\Contracts;

/**
 * Interface EntityContract
 * @package App\Domain\EspoCRM\Contracts
 */
interface EntityContract {

    /**
     * @param array $data
     * @return string returns the entity id
     */
    public function create( array $data );

    /**
     * @param $id
     * @param array $data
     * @return bool
     */
    public function update( $id, array $data );

    /**
     * @param $id
     * @return bool whether the entity was deleted or not
     */
    public function delete( $id );

    /**
     * @param $id
     * @return mixed
     */
    public function read( $id );

    /**
     * @return mixed
     */
    public function index();
}
