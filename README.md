# EspoCRM Laravel Client #

A simple package for your laravel application to communicate with an EspoCRM instance over their API.

### How to install ###
Install via composer, add this repo to the repositories block of your composer.json, then specify dawisko1/espocrm-laravel-client in the require block.

### Configuration ###

Copy the following to your .env file:

```
# Your EspoCRM URL goes here (without trailing '/')
ESPOCRM_URL=[YOUR_ESPOCRM_URL]

# Specify the authentication type, supported are: HMAC, API, BASIC
ESPOCRM_AUTH_TYPE="HMAC"

# Your EspoCRM User API Key
ESPOCRM_KEY=[YOUR_USER_API_KEY]

# Your EspoCRM User API Secret Key (HMAC only)
ESPOCRM_SECRET=[YOUR_USER_SECRET_KEY]
```

Checkout the config/espocrm.php for more configuration options.

#### Note on Authentication ####
The method you select in .env must be the same you selected in the API User config of your EspoCRM.

**Note: BASIC Authentication Method is discouraged.**

### Usage ###
Using the package is quite simple, you can make your requests directly from the EspoCRM\Client or use pre-made Entity classes for ease.

#### Entity Classes ####
Currently there are only 5 Entity types, these are:

* Call
* Meeting
* Campaign
* Issue (Case in EspoCRM terminology)
* Task

**You'll find them in the ```LaravelEspo\Entities``` namespace.**

#### Entity Methods ####

Each entity provides static methods for ease of use:
* ```new( Array ): String```
* ```get( String ): Array```
* ```amend( String, Array ): Boolean```
* ```destroy( String ): Boolean```
* ```list( Void ): Array```

```php
// Getting a task
$task = Task::get("5fe4bae7e9ce14d3f");

// Creating a task
$taskId = Task::new([
    "name" => "Some task name",
]);
```
*<sub>Note: the rest of the fields like accountId (usually repetitive) should be defined in espocrm defaults config.</sub>*

#### Entity Default Fields ####
In espocrm.php (espocrm.settings.defaults to be exact) you can specify default fields for the client as well as per entity, 
per method. It's quite a powerful feature which not only reduces possible human errors, but saves you lots of time and 
repetitiveness in your code, do check it out.

### Custom Entities ###
If you wish to create a custom entity, simply extend the ```LaravelEspo\Entities\Entity``` class, it provides all the necessary functionality so
you can focus on your own logic rather than boilerplate.

### Who do I talk to? ###
Need help? or want to chat or maybe you have a suggestion or issue?
All of those and more are welcome, you can reach me at [d.s.h12421@gmail.com](mailto:d.s.h12421@gmail.com "Do use meaningful subjects tho :D").

* Repo owner at Dawid Skowroński<d.s.h12421@gmail.com> (a.k.a.: dawisko1)